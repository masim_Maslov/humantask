import java.util.Calendar;
import java.util.Objects;

public class Human implements Comparable<Human>{
    private String name;
    private String surname;
    private int age;
    private Gender gender;
    private Calendar birthDate;

    public Human(String name, String surname, Gender gender) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human() {
        this("name", "surname");
    }

    public void name(String name) {
        this.name = name;
    }

    public void surname(String surname) {
        this.surname = surname;
    }

    public void age(int age) {
        this.age = age;
    }

    public void gender(Gender gender) {
        this.gender = gender;
    }

    public void birthDate (Calendar birthDate) {
        this.birthDate = (Calendar)birthDate.clone();
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    @Override
    public String toString() {
        return "Name: " + name + " Surname: " + surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                name.equals(human.name) &&
                surname.equals(human.surname) &&
                gender == human.gender &&
                birthDate.equals(human.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, age, gender, birthDate);
    }

    @Override
    public int compareTo(Human human) {
        return -getBirthDate().compareTo(human.getBirthDate());
    }
}
