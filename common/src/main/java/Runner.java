import java.text.SimpleDateFormat;
import java.util.*;

public class Runner {
    public static void main(String[] args) {
        Human testHuman = HumanCreator.createHuman();
        System.out.println(testHuman);
        testHuman = HumanCreator.createHuman("Maks", "Maksov");
        System.out.println(testHuman);
        testHuman = HumanCreator.createHuman("Maks", "Maksov", Gender.MALE);
        System.out.println(testHuman);

        List<Human> humanList = new LinkedList<Human>();
        Human newHuman = new Human("Ivan", "Dirkov", Gender.MALE);
        newHuman.age(16);
        newHuman.birthDate(new GregorianCalendar(1998, Calendar.MAY, 7));
        humanList.add(newHuman);
//        System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(newHuman.getBirthDate().getTime()));

        Human newHuman1 = new Human("Ivan", "Dirkov", Gender.MALE);
        newHuman1.age(16);
        newHuman1.birthDate(new GregorianCalendar(1998, Calendar.MAY, 6));
        humanList.add(newHuman1);

        Human newHuman2 = new Human("Ivan", "Dirkov", Gender.FEMALE);
        newHuman2.age(16);
        newHuman2.birthDate(new GregorianCalendar(1986, Calendar.FEBRUARY, 6));
        humanList.add(newHuman2);

        Human newHuman3 = new Human("Ivan", "Sirkov3", Gender.MALE);
        newHuman3.age(16);
        newHuman3.birthDate(new GregorianCalendar(1947, Calendar.APRIL, 6));
        humanList.add(newHuman3);

        Human newHuman4 = new Human("Ivan", "Dirkov4", Gender.FEMALE);
        newHuman4.age(16);
        newHuman4.birthDate(new GregorianCalendar(2001, Calendar.MAY, 6));
        humanList.add(newHuman4);

        Human newHuman5 = new Human("Ivan", "Kirkov5", Gender.MALE);
        newHuman5.age(16);
        newHuman5.birthDate(new GregorianCalendar(1977, Calendar.MARCH, 6));
        humanList.add(newHuman5);

        Human newHuman6 = new Human("Ivan", "Kirkov6", Gender.FEMALE);
        newHuman6.age(16);
        newHuman6.birthDate(new GregorianCalendar(1933, Calendar.MAY, 6));
        humanList.add(newHuman6);

        Human newHuman7 = new Human("Ivan", "Sirkov7", Gender.MALE);
        newHuman7.age(16);
        newHuman7.birthDate(new GregorianCalendar(1998, Calendar.FEBRUARY, 6));
        humanList.add(newHuman7);

        Human newHuman8 = new Human("Ivan", "Dirkov8", Gender.FEMALE);
        newHuman8.age(16);
        newHuman8.birthDate(new GregorianCalendar(1998, Calendar.MAY, 6));
        humanList.add(newHuman8);

        System.out.println(newHuman.equals(newHuman1));

        HumansTasks.findCoincidence(humanList);
        Set<Human> huhmans = HumansTasks.findCoincidenceInCollections(humanList);
        if (huhmans.size() != 0) {
            System.out.println("Что то мы все таки нашли");
        }

        for (Human h : huhmans) {
            System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(h.getBirthDate().getTime()));
        }

    }
}
