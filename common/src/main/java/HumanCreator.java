
public class HumanCreator {
    public static Human createHuman() {
        return new Human();
    }

    public static Human createHuman(String name, String surname) {
        return new Human(name, surname);
    }

    public static Human createHuman(String name, String surname, Gender gender) {
        return new Human(name, surname, gender);
    }

}
