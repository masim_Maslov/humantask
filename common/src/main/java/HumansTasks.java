import java.util.*;

public class HumansTasks {
    //совпадение двух людей
    public static boolean findCoincidenceByBirthDate(List<Human> humanList, Human template) {
        return humanList.contains(template);
    }

    //вывод младше 20 лет и совпадение по буквам
    public static void findCoincidence(List<Human> humanList) {
        Set<Character> set = new HashSet<Character>();
        set.add('a');
        set.add('b');
        set.add('c');
        set.add('d');
        set.add('e');

        Calendar currentCalendar = new GregorianCalendar();

        for(Human h : humanList) {
            int diff = currentCalendar.get(Calendar.YEAR) - h.getBirthDate().get(Calendar.YEAR);
            if (diff <= 20) {
                if (set.contains(h.getSurname().toLowerCase().charAt(0))) {
                    System.out.println(h);
                }
            }
        }
    }

    //вывести повторяющиеся объекты в след виде {1: {human}} - повторяющиеся - совпадает имя и фамилия
    public static Set<Human> findCoincidenceInCollections(List<Human> humanList) {

        Set<Human> resultSet = new TreeSet<Human>();

        Map<String, List<Human>> map = new HashMap<String, List<Human>>();
        for (Human h: humanList) {
            String key = (h.getName() + h.getSurname()).toLowerCase();
            List<Human> value = map.get(key);
            if (value != null) {
                value.add(h);
                map.put(key, value);
            } else {
                value = new LinkedList<Human>();
                value.add(h);
                map.put(key,value);
            }
        }

        Set<String> nameSets = map.keySet();
        for (String currentKey : nameSets) {
            List<Human> currentList= map.get(currentKey);
            System.out.printf("Size of list %d \n", currentList.size());
            if (currentList.size()  <= 2) {
                currentList.sort((Human h1, Human h2) -> {
                    return h1.getBirthDate().compareTo(h2.getBirthDate());
                });
                for (Human h : currentList) {
                    System.out.println(h);
                }
            }
            else {
                    resultSet.addAll(currentList);
            }
        }

        return resultSet;
    }

}
